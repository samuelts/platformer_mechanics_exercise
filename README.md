# platformer_mechanics_exercise
An exercise in writing platformer mechanics and animation code

# Current Status
Player movement mechanics and animation are about 90% complete, a few bugs remain such as sliding animation facing the wrong direction if player turns and moves away from wall but not far enough to stop raycast collision detection, and the dash animation persisting on collision with walls before dash timer expires.

# Images
![Idle](https://i.imgur.com/oK581MT.png)

![Fall](https://i.imgur.com/xkEnzaD.png)
