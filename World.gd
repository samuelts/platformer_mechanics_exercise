extends Node

onready var player = preload("res://Player.tscn")
onready var playerNode = get_node("Player")
onready var playerPos = playerNode.get_position()
onready var playerOrigin = playerPos

func _physics_process(delta):
	playerPos = playerNode.get_position()
	if playerPos.y > 4000:
		playerNode.set_position(playerOrigin)