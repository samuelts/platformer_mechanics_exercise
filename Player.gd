extends KinematicBody2D

const GRAVITY_VEC = Vector2(0, 900)
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
const MIN_ONAIR_TIME = 0.1
const WALK_SPEED = 250 # pixels/sec
const DASH_SPEED = 25
const JUMP_SPEED = 400
const WALL_SLIDE_SPEED = 50
const SIDING_CHANGE_SPEED = 10

var linear_vel = Vector2()
var wall_normal = Vector2()
var onair_time = 0
var on_floor = false
var doublejump = false
var walljump = false
var hascontrol = true
var left_wall = false
var right_wall = false
var dash_active = false
var can_dash = true
var control_timer = 0
var attack = 0
var attack_iter = 0
var attack_reset = 0
var dash_reset = 0

var anim = ""
var new_anim = ""

var attack_state = null

onready var sprite = $playerSprite
onready var ground_ray = $ground_ray
onready var left_ray = $left_ray
onready var right_ray = $right_ray

enum ATTACK_STATE {
	IDLE1,
	IDLE2,
	ATTACK1,
	ATTACK2,
	ATTACK3	
}

#cache the sprite here for fast access (we will set scale to flip it often)
func _ready():	
	
	sprite.connect("animation_finished", self, "attack_anim_finished")
	
func attack_anim_finished():
	
	if (attack_state == ATTACK_STATE.ATTACK1):
		sprite.play("attack1")
		attack_state = ATTACK_STATE.IDLE2
	elif (attack_state == ATTACK_STATE.ATTACK2):
		sprite.play("attack2")
		attack_state = ATTACK_STATE.IDLE2
	elif (attack_state == ATTACK_STATE.ATTACK3):
		sprite.play("attack3")
		attack_state = ATTACK_STATE.IDLE2
	elif (attack_state == ATTACK_STATE.IDLE2):
		sprite.play("idle2")
		attack = 0
	else:
		pass

func _physics_process(delta):
	
	### RESET STATES/INCREMENT COUNTERS ###
	if (attack == 0):
		attack_state = ATTACK_STATE.IDLE1
	
	onair_time += delta
	
	#Control timer for player control
	if control_timer > 0:
		control_timer -= 1
		hascontrol = false
	else:
		control_timer = 0
		hascontrol = true
		
	# If no attack reset attack to idle state
	if attack_reset > 0:
		attack_reset -= 1
	else:
		attack_reset = 0
		attack_iter = 0
		
	# If dash_active too long set false
	if dash_reset > 0:
		dash_reset -= 1
	else:
		dash_reset = 0
		dash_active = false
	
	### MOVEMENT ###
	
	# Detect Wall
	if left_ray.is_colliding():
		left_wall = true
	elif right_ray.is_colliding():
		right_wall = true
	else:
		right_wall = false
		left_wall = false
		
	# Detect Floor
	if ground_ray.is_colliding():
		onair_time = 0
		doublejump = true
		hascontrol = true
		can_dash = true
	
	on_floor = onair_time < MIN_ONAIR_TIME
		
	# Apply Gravity
	if (left_wall || right_wall) && linear_vel.y > 0:
		linear_vel.y += 3
	elif dash_active:
		linear_vel.y = 0
	else:
		linear_vel += delta * GRAVITY_VEC
		
	# Move and Slide
	linear_vel = move_and_slide(linear_vel, FLOOR_NORMAL, SLOPE_SLIDE_STOP)
	
	### ATTACKS ###
	
	# Left Mouse Attack Increments Attack 1 then 2 then 3
	if Input.is_action_just_pressed("mouse_left"):
		attack = attack_iter + 1
		if attack > 3: attack = 1
		attack_iter = attack
		attack_reset = 60
	
	### CONTROL ###
	
	# Horizontal Movement
	var target_speed = 0
	if hascontrol:
		if Input.is_action_pressed("move_left"):
			target_speed += -1
		if Input.is_action_pressed("move_right"):
			target_speed +=  1
		if Input.is_action_just_pressed("dash") && can_dash:
			target_speed *= DASH_SPEED
			linear_vel.x = target_speed
			linear_vel.y = 0
			dash_active = true
			can_dash = false
			dash_reset = 30
	
	target_speed *= WALK_SPEED
	linear_vel.x = lerp(linear_vel.x, target_speed, 0.1)
	
	# Jumping
	if on_floor and Input.is_action_just_pressed("jump"):
		linear_vel.y = -JUMP_SPEED
		#$sound_jump.play()
	# Wall Jump off Right
	elif right_wall and !on_floor and Input.is_action_just_pressed("jump"):
		linear_vel.y = -JUMP_SPEED
		linear_vel.x = lerp(linear_vel.x, -2*JUMP_SPEED, 0.5)
		control_timer = 15
		doublejump = true
		walljump = true
	# Wall Jump off Left
	elif left_wall and !on_floor and Input.is_action_just_pressed("jump"):
		linear_vel.y = -JUMP_SPEED
		linear_vel.x = lerp(linear_vel.x, 2*JUMP_SPEED, 0.5)
		control_timer = 15
		doublejump = true
		walljump = true
	# Double jump
	elif doublejump and Input.is_action_just_pressed("jump"):
		doublejump = false
		linear_vel.y = -JUMP_SPEED
	
	### ANIMATION ###
	
	if attack_state == ATTACK_STATE.IDLE1:
		
		# Default animation
		new_anim = "idle1"
		
		if on_floor:
			if linear_vel.x < -SIDING_CHANGE_SPEED:
				sprite.scale.x = -1
				new_anim = "run"
			
			if linear_vel.x > SIDING_CHANGE_SPEED:
				sprite.scale.x = 1
				new_anim = "run"
				
			# Slide animation
			if target_speed == 0 and linear_vel.x > 125:
				new_anim = "slide"
			elif target_speed == 0 and linear_vel.x != 0:
				new_anim = "stand"
		else:
			# We want the character to immediately change facing side when the player
			# tries to change direction, during air control.
			# This allows for example the player to shoot quickly left then right.
			if Input.is_action_pressed("move_left") and not Input.is_action_pressed("move_right"):
				sprite.scale.x = -1
			if Input.is_action_pressed("move_right") and not Input.is_action_pressed("move_left"):
				sprite.scale.x = 1
			
			if (left_wall || right_wall):
				new_anim = "grabdirt"
			elif (linear_vel.y < 0) && (!doublejump || walljump):
				new_anim = "smrslt"
			elif linear_vel.y < 0:
				new_anim = "jump"
			else:
				new_anim = "fall"
		
		# Dash animation
		if dash_active:
			new_anim = "slide"
		
		# Set Attack Animation based on Attack State
		if attack == 1:
			attack_state = ATTACK_STATE.ATTACK1
		elif attack == 2:
			attack_state = ATTACK_STATE.ATTACK2
		elif attack == 3:
			attack_state = ATTACK_STATE.ATTACK3
		else:
			attack_state = ATTACK_STATE.IDLE1

		# Update animation
		if new_anim != anim:
			anim = new_anim
			sprite.play(anim)